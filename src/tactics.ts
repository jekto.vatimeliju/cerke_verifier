import { AbsoluteCoord, Color, Profession } from "cerke_online_api";
import { isTamHue } from "./calculate_movable";
import { rotateField } from "./game_state";
import { PureGameState, PureOpponentMove, fromAbsoluteCoord_, not_from_hand_candidates } from "./pure";
import { coordEq, NonTam2Piece, NonTam2PieceDownward, Piece } from "./type__piece";
import { Hand, ObtainablePieces, calculate_hands_and_score_from_pieces } from "cerke_hands_and_score";

function toObtainablePiece(color: Color, prof: Profession): ObtainablePieces {
    const a: ObtainablePieces[][] = [
        [
            "赤船",
            "赤兵",
            "赤弓",
            "赤車",
            "赤虎",
            "赤馬",
            "赤筆",
            "赤巫",
            "赤将",
            "赤王",
        ],
        [
            "黒船",
            "黒兵",
            "黒弓",
            "黒車",
            "黒虎",
            "黒馬",
            "黒筆",
            "黒巫",
            "黒将",
            "黒王",
        ],
    ];
    return a[color][prof];
}

function calculateHandsAndScore(pieces: NonTam2Piece[]) {
    const hop1zuo1: ObtainablePieces[] = pieces.map(p =>
        toObtainablePiece(p.color, p.prof),
    );
    const res:
        | { error: false; score: number; hands: Hand[] }
        | {
            error: true;
            too_many: ObtainablePieces[];
        } = calculate_hands_and_score_from_pieces(hop1zuo1);
    if (res.error === true) {
        throw new Error(`should not happen: too many of ${res.too_many.join(",")}`);
    }

    return { hands: res.hands, score: res.score };
}

export function is_victorious_hand(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): boolean {
    if (cand.type === "TamMove") {
        return false;
    } else if (cand.type === "NonTamMove") {
        if (cand.data.type === "FromHand") {
            return false;
        } else {
            if (coordEq_absolute(cand.data.dest, cand.data.src)) {
                // self-occlusion
                return false;
            }
            const dest_occupied_by = getPiece(pure_game_state, cand.data.dest);

            // cannot win if a piece was not obtained
            if (!dest_occupied_by) { return false; }
            if (dest_occupied_by === "Tam2") { throw new Error("tam cannot be captured, why is it in the destination?") }

            const old_calc = calculateHandsAndScore(pure_game_state.f.hop1zuo1OfDownward);

            const new_calc = calculateHandsAndScore([...pure_game_state.f.hop1zuo1OfDownward, dest_occupied_by]);

            return (new_calc.score !== old_calc.score)
        }
    } else if (cand.type === "InfAfterStep") {
        const dest_occupied_by = getPiece(pure_game_state, cand.plannedDirection);

        // cannot win if a piece was not obtained
        if (!dest_occupied_by) { return false; }
        if (dest_occupied_by === "Tam2") { throw new Error("tam cannot be captured, why is it in the destination?") }
        if (coordEq_absolute(cand.plannedDirection, cand.src)) {
            // self-occlusion
            return false;
        }

        const old_calc = calculateHandsAndScore(pure_game_state.f.hop1zuo1OfDownward);

        const new_calc = calculateHandsAndScore([...pure_game_state.f.hop1zuo1OfDownward, dest_occupied_by]);

        return (new_calc.score !== old_calc.score)
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}

// Exporting this clashes with that of pure
function isWater([row, col]: AbsoluteCoord): boolean {
    return (
        (row === "O" && col === "N") ||
        (row === "O" && col === "T") ||
        (row === "O" && col === "Z") ||
        (row === "O" && col === "X") ||
        (row === "O" && col === "C") ||
        (row === "I" && col === "Z") ||
        (row === "U" && col === "Z") ||
        (row === "Y" && col === "Z") ||
        (row === "AI" && col === "Z")
    );
}

export function distance(a: AbsoluteCoord, b: AbsoluteCoord): number {
    const [x1, y1] = fromAbsoluteCoord_(a, true);
    const [x2, y2] = fromAbsoluteCoord_(b, true);
    return Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2));
}
function coordEq_absolute(a: AbsoluteCoord, b: AbsoluteCoord): boolean {
    return coordEq(fromAbsoluteCoord_(a, true), fromAbsoluteCoord_(b, true))
}

// 「入水判定が要らず、3以下の踏越え判定しか要らない」を「やりづらくはない(likely to succeed)」と定義する。
export function is_likely_to_succeed(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): boolean {
    if (cand.type === "TamMove") { return true; }
    else if (cand.type === "NonTamMove" && cand.data.type === "FromHand") { return true; }
    else if (cand.type === "NonTamMove") { return !isWater(cand.data.dest); }
    else if (cand.type === "InfAfterStep") {
        return distance(cand.plannedDirection, cand.step) <= 3;
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}


// 「入水判定が要らず、2以下の踏越え判定しか要らない」を「やりやすい(very likely to succeed)」と定義する。
export function is_very_likely_to_succeed(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): boolean {
    if (cand.type === "TamMove") { return true; }
    else if (cand.type === "NonTamMove" && cand.data.type === "FromHand") { return true; }
    else if (cand.type === "NonTamMove") { return !isWater(cand.data.dest); }
    else if (cand.type === "InfAfterStep") {
        return distance(cand.plannedDirection, cand.step) <= 2;
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}

function setPiece(pgs: PureGameState, coord: AbsoluteCoord, piece: Piece | null) {
    const [i, j] = fromAbsoluteCoord_(coord, pgs.IA_is_down);
    pgs.f.currentBoard[i][j] = piece;
}

function getPiece(pgs: Readonly<PureGameState>, coord: AbsoluteCoord): Piece | null {
    const [i, j] = fromAbsoluteCoord_(coord, pgs.IA_is_down);
    return pgs.f.currentBoard[i][j];
}


function removeFromHop1Zuo1OfDownward(
    game_state: PureGameState,
    color: Color,
    prof: Profession,
): NonTam2PieceDownward {
    const ind = game_state.f.hop1zuo1OfDownward.findIndex(
        p => p.color === color && p.prof === prof,
    );
    if (ind === -1) {
        throw new Error("What should exist in the hand does not exist");
    }
    const [removed] = game_state.f.hop1zuo1OfDownward.splice(ind, 1);
    return removed;
}

// 入水や踏越え判定はすべて成功するものとする
function apply_opponent_move_assuming_every_luck_works(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): PureGameState {
    const pgs: PureGameState = JSON.parse(JSON.stringify(pure_game_state)); // deep clone
    if (cand.type === "TamMove") {
        setPiece(pgs, cand.src, null);
        setPiece(pgs, cand.secondDest, "Tam2");
        return pgs;
    } else if (cand.type === "NonTamMove") {
        if (cand.data.type === "SrcDst" || cand.data.type === "SrcStepDstFinite") {
            const piece = getPiece(pgs, cand.data.src);
            setPiece(pgs, cand.data.src, null);
            setPiece(pgs, cand.data.dest, piece);
            return pgs;
        } else if (cand.data.type === "FromHand") {
            const piece = removeFromHop1Zuo1OfDownward(pgs, cand.data.color, cand.data.prof);
            setPiece(pgs, cand.data.dest, piece);
            return pgs;
        } else {
            const _should_not_reach_here: never = cand.data;
            throw new Error("does not happen");
        }
    } else if (cand.type === "InfAfterStep") {
        const piece = getPiece(pgs, cand.src);
        setPiece(pgs, cand.plannedDirection, piece);
        return pgs;
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}

export function apply_and_rotate(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): PureGameState {
    const pgs = apply_opponent_move_assuming_every_luck_works(cand, pure_game_state);

    return rotateGameState(pgs, cand.type === "TamMove");
}

function rotateGameState(pgs: Readonly<PureGameState>, opponent_has_just_moved_tam: boolean): PureGameState {
    return {
        f: rotateField(pgs.f),
        IA_is_down: !pgs.IA_is_down,
        tam_itself_is_tam_hue: pgs.tam_itself_is_tam_hue,
        opponent_has_just_moved_tam
    }
}

function gak_tuk_newly_generated(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): AbsoluteCoord | null {
    const is_tam_hue = (dest: AbsoluteCoord) => isTamHue(
        fromAbsoluteCoord_(dest, pure_game_state.IA_is_down),
        pure_game_state.f.currentBoard,
        pure_game_state.tam_itself_is_tam_hue
    );
    if (cand.type === "TamMove") {
        return null;
    } else if (cand.type === "NonTamMove") {
        if (cand.data.type === "FromHand") {
            if (cand.data.prof !== Profession.Tuk2) { return null; }
            if (!is_tam_hue(cand.data.dest)) { return null; }
            return cand.data.dest;
        } else {
            const src_piece = getPiece(pure_game_state, cand.data.src);
            if (src_piece === "Tam2") { throw new Error("Well, that should be TamMove") }
            if (src_piece?.prof !== Profession.Tuk2) { return null; }
            if (
                /* 結果として激巫が無い */ !is_tam_hue(cand.data.dest)
                || /* もとから激巫だった */ is_tam_hue(cand.data.src)) {
                return null;
            }

            return cand.data.dest;
        }
    } else if (cand.type === "InfAfterStep") {
        const [i, j] = fromAbsoluteCoord_(cand.src, pure_game_state.IA_is_down);
        const src_piece = pure_game_state.f.currentBoard[i][j];
        if (src_piece === "Tam2") { throw new Error("Well, that should be TamMove") }
        if (src_piece?.prof !== Profession.Tuk2) { return null; }
        if (
            /* 結果として激巫が無い */ !is_tam_hue(cand.plannedDirection)
            || /* もとから激巫だった */ is_tam_hue(cand.src)) {
            return null;
        }

        return cand.plannedDirection;
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}

// 取られづらい激巫が作られているかを確認
export function is_safe_gak_tuk_newly_generated(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): boolean {
    const tuk_coord = gak_tuk_newly_generated(cand, pure_game_state);
    if (!tuk_coord) {
        return false;
    }

    const next = apply_and_rotate(cand, pure_game_state);
    const candidates = not_from_hand_candidates(next);

    const countermeasures_exist = candidates.some(cand => {
        // 行いづらい？
        if (!is_likely_to_succeed(cand, next)) { return false; }

        // それは tuk_coord を侵害する？
        if (cand.type === "TamMove") {
            return false;
        } else if (cand.type === "NonTamMove") {
            if (cand.data.type === "FromHand") { return false; }
            else { return coordEq_absolute(tuk_coord, cand.data.dest); }
        } else if (cand.type === "InfAfterStep") {
            return coordEq_absolute(cand.plannedDirection, tuk_coord);
        } else {
            const _should_not_reach_here: never = cand;
            throw new Error("does not happen");
        }
    });

    return !countermeasures_exist;
}

function isOccupied(dest: AbsoluteCoord, pure_game_state: Readonly<PureGameState>): boolean {
    const [i, j] = fromAbsoluteCoord_(dest, pure_game_state.IA_is_down);
    return pure_game_state.f.currentBoard[i][j] !== null;
}

export function if_capture_get_coord(cand: Readonly<PureOpponentMove>, pure_game_state: Readonly<PureGameState>): AbsoluteCoord | null {
    if (cand.type === "TamMove") {
        return null;
    } else if (cand.type === "NonTamMove") {
        if (cand.data.type === "FromHand") {
            return null;
        } else if (cand.data.type === "SrcDst") {
            if (isOccupied(cand.data.dest, pure_game_state)) { return cand.data.dest; }
            else { return null; }
        } else if (cand.data.type === "SrcStepDstFinite") {
            // self-occlusion possible
            if (coordEq_absolute(cand.data.src, cand.data.dest)) { return null; }
            else if (isOccupied(cand.data.dest, pure_game_state)) { return cand.data.dest; }
            else { return null; }
        } else {
            const _should_not_reach_here: never = cand.data;
            throw new Error("does not happen");
        }
    } else if (cand.type === "InfAfterStep") {
        // self-occlusion possible
        if (coordEq_absolute(cand.src, cand.plannedDirection)) { return null; }
        else if (isOccupied(cand.plannedDirection, pure_game_state)) { return cand.plannedDirection; }
        else { return null; }
    } else {
        const _should_not_reach_here: never = cand;
        throw new Error("does not happen");
    }
}

